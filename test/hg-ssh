#!/bin/sh

# Copyright 2017 Bob Proulx <bob@proulx.com>
# You may freely use, modify and/or distribute this file.

# Ensure a standard behavior regardless of caller locale setting.
export LC_ALL=C

if [ -z "$SAVANNAH_USER" ]; then
  echo "Skipping: SAVANNAH_USER not set" 1>&2
  exit 77
fi

if ! (hg --version) >/dev/null 2>&1; then
  echo "Error: Missing hg" 1>&2
  exit 77
fi

if ! (ssh -V) >/dev/null 2>&1; then
  echo "Error: Missing ssh" 1>&2
  exit 77
fi

unset tmpdir
cleanup() {
  test -n "$tmpdir" && rm -rf "$tmpdir" && unset tmpdir
}
trap "cleanup" EXIT
trap "cleanup; trap - HUP; kill -HUP $$" HUP
trap "cleanup; trap - INT; kill -INT $$" INT
trap "cleanup; trap - QUIT; kill -QUIT $$" QUIT
trap "cleanup; trap - TERM; kill -TERM $$" TERM

tmpdir=$(mktemp -d -t "sv-test.XXXXXXXX") || exit 1

cd "$tmpdir" || exit 1

testit() {
  url=$1 ; shift
  if [ $# -ge 1 ]; then
    args=$1 ; shift
  fi
  hg clone "$url"
  if [ $? -ne 0 ]; then
    echo "Failed to hg clone $url"
    exit 1
  fi
  base=$(basename "$url")
  test -d "$base" || exit 1
  rm -rf "$base"
  test -d "$base" && exit 1
}

list="
ssh://$SAVANNAH_USER@hg.savannah.gnu.org/test-project
ssh://$SAVANNAH_USER@hg.savannah.nongnu.org/test-project
ssh://$SAVANNAH_USER@hg.sv.gnu.org/test-project
ssh://$SAVANNAH_USER@hg.sv.nongnu.org/test-project
"

for url in $list; do
  testit "$url"
done

exit 0
