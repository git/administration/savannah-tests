#!/bin/sh

# Copyright 2017 Bob Proulx <bob@proulx.com>
# You may freely use, modify and/or distribute this file.

# Ensure a standard behavior regardless of caller locale setting.
export LC_ALL=C

if ! (rsync --version) >/dev/null 2>&1; then
  echo "Error: Missing rsync" 1>&2
  exit 77
fi

unset tmpdir
cleanup() {
  test -n "$tmpdir" && rm -rf "$tmpdir" && unset tmpdir
}
trap "cleanup" EXIT
trap "cleanup; trap - HUP; kill -HUP $$" HUP
trap "cleanup; trap - INT; kill -INT $$" INT
trap "cleanup; trap - QUIT; kill -QUIT $$" QUIT
trap "cleanup; trap - TERM; kill -TERM $$" TERM

tmpdir=$(mktemp -d -t "sv-test.XXXXXXXX") || exit 1

cd "$tmpdir" || exit 1

testit() {
  url=$1 ; shift
  rsync "$url" > out
  if [ $? -ne 0 ]; then
    echo "Failed: rsync $url"
    exit 1
  fi
  test -s out || exit 1
  rm -f out
  test -s out && exit 1
}

testit rsync://cvs.savannah.gnu.org/sources/test-project/
testit rsync://cvs.savannah.nongnu.org/sources/test-project/
testit rsync://cvs.sv.gnu.org/sources/test-project/
testit rsync://cvs.sv.nongnu.org/sources/test-project/

exit 0
