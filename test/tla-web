#!/bin/sh

# Copyright 2019 Bob Proulx <bob@proulx.com>
# You may freely use, modify and/or distribute this file.

# Ensure a standard behavior regardless of caller locale setting.
export LC_ALL=C

if ! (wget --version) >/dev/null 2>&1; then
  echo "Error: Missing wget" 1>&2
  exit 77
fi

unset tmpdir
cleanup() {
  test -n "$tmpdir" && rm -rf "$tmpdir" && unset tmpdir
}
trap "cleanup" EXIT
trap "cleanup; trap - HUP; kill -HUP $$" HUP
trap "cleanup; trap - INT; kill -INT $$" INT
trap "cleanup; trap - QUIT; kill -QUIT $$" QUIT
trap "cleanup; trap - TERM; kill -TERM $$" TERM

tmpdir=$(mktemp -d -t "sv-test.XXXXXXXX") || exit 1

cd "$tmpdir" || exit 1

testit() {
  url=$1 ; shift
  wget -Oout "$url"
  if [ $? -ne 0 ]; then
    echo "Failed to web GET: $url"
    exit 1
  fi
  test -s out || exit 1
  rm -f out
  test -s out && exit 1
}

list="
//arch.savannah.gnu.org/archives/gnu-arch/
//arch.savannah.nongnu.org/archives/gnu-arch/
//arch.sv.gnu.org/archives/gnu-arch/
//arch.sv.nongnu.org/archives/gnu-arch/
"
for url in $list; do
  testit "https:$url"
done
for url in $list; do
  testit "http:$url"
done
list="
http://arch.savannah.gnu.org/
https://arch.savannah.gnu.org/
"
for url in $list; do
  testit "$url"
done
list="
http://arch.savannah.gnu.org/.well-known
http://arch.savannah.gnu.org/.well-known/
http://arch.savannah.nongnu.org/.well-known
http://arch.savannah.nongnu.org/.well-known/
http://arch.sv.gnu.org/.well-known
http://arch.sv.gnu.org/.well-known/
http://arch.sv.nongnu.org/.well-known
http://arch.sv.nongnu.org/.well-known/
"
for url in $list; do
  testit "$url"
done

testrobots() {
  url=$1 ; shift
  wget -Oout "$url"
  fail=false
  if [ $? -ne 0 ]; then
    echo "Failed to web GET: $url"
    fail=true
  fi
  test -s out || fail=true
  grep ^User-agent: out || fail=true
  grep ^Disallow: out || fail=true
  if $fail; then
    cat out
    exit 1
  fi
  rm -f out
  test -s out && fail=true
  $fail && exit 1
}

testrobots https://arch.savannah.gnu.org/robots.txt
testrobots https://arch.savannah.nongnu.org/robots.txt

testrobots https://arch.sv.gnu.org/robots.txt
testrobots https://arch.sv.nongnu.org/robots.txt

testrobots http://arch.savannah.gnu.org/robots.txt
testrobots http://arch.savannah.nongnu.org/robots.txt

testrobots http://arch.sv.gnu.org/robots.txt
testrobots http://arch.sv.nongnu.org/robots.txt

exit 0
